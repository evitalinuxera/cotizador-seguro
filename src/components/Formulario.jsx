import React, {useState} from 'react';
import styled from '@emotion/styled';
import { obtenerDiferenciaYear, calcularMarca, obtenerPlan } from '../helper';

const Campo = styled.div`
    display: flex;
    margin-bottom: 1rem;
    align-items: center;
`;
const Label = styled.label`
    flex: 0 0 100px;
`;
const Select = styled.select`
    display: block;
    width: 100%;
    padding: 1rem;
    border: 1px solid #e1e1e1;
    -webkit-appearance: none;
`;
const Input = styled.input`
    margin: 0 1rem;
`;
const Boton = styled.button`
    background-color: #00838f;
    font-size: 16px;
    width: 100%;
    padding: 1rem;
    color: #FFF;
    text-transform: uppercase;
    font-weight: bold;
    border: none;
    transition: background-color .3s ease;
    margin-top: 2rem;

    &:hover{
        cursor: pointer;
        background-color: #26C6DA;
    }
`;

const Error = styled.div`
    background-color: red;
    color: white;
    padding: 1rem;
    width: 100%;
    text-align: center;
    margin-bottom: 3rem;
`;

const Formulario = ( {guardarResumen, guardarCargando} ) => {

// Hago el state para leer el form
const [datos, guardarDatos] = useState({
    marca: '',
    year: '',
    plan: '',
});

// Hago el state de error

const [error, guardarError] = useState(false);

// Extraigo valores
const {marca, year, plan} = datos;

// Leer los datos del form
const obtenerInformacion = e => {
    guardarDatos({
        ...datos,
        [e.target.name] : e.target.value
    })
};

const cotizarSeguro = e => {
    // Prevengo el default
    e.preventDefault();

    // Valido el formulario
    if (marca.trim() === "" || year.trim() === "" || plan.trim() === ""){
        guardarError(true);
        return;
    };

    // Repongo el error
    guardarError(false);

    // Pongo el precio base
    let resultado = 2000;

    // Obtener la diferencia de años
    const diferencia = obtenerDiferenciaYear(year);
    
    // Por cada año hay que restar 3%
    resultado -= ((diferencia * 3) * resultado) / 100;

    // Americano 15%
    // Asiático 5%
    // Europeo 30%
    resultado = calcularMarca(marca) * resultado;

    // Básico aumenta 20%
    // Completo aumenta 50%
    const incrementoPlan = obtenerPlan(plan);
    resultado = parseFloat (incrementoPlan * resultado).toFixed(2);

    // Gestiono el spinner
    guardarCargando(true);
    setTimeout(() => {
        // Paso el cargando a false
        // para eliminar el spinner
        guardarCargando(false);

         // Corrijo el state que traje
         // y lo paso al componenente ppal
            guardarResumen({
                cotizacion: resultado,
                datos
            })
            }, 2000);
   ;
};


    return ( 
        <form
            onSubmit={cotizarSeguro}
        >
            { error 
            ? <Error>
                Todos los campos deben completarse
            </Error>
            : null }
            <Campo>
                <Label>Marca:</Label>
                <Select
                    name="marca"
                    value={marca}
                    onChange={obtenerInformacion}
                >
                    <option value=""> -- Seleccione -- </option>
                    <option value="americano">USA </option>
                    <option value="europeo">Europeo</option>
                    <option value="asiatico">Asiático</option>
                </Select>
            </Campo>
            <Campo>
                <Label>Año:</Label>
                <Select
                    name="year"
                    value={year}
                    onChange={obtenerInformacion}
                >
                    <option value="">-- Seleccione --</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                </Select>
            </Campo>
            <Campo>
                <Label>Plan:</Label>
                <Input 
                    type="radio"
                    name="plan"
                    value="basico"
                    checked={plan === "basico"}
                    onChange={obtenerInformacion}
                />Básico
                <Input 
                    type="radio"
                    name="plan"
                    value="completo"
                    checked={plan === "completo"}
                    onChange={obtenerInformacion}
                />Completo
            </Campo>
            <Boton
                type="submit"    
            >
                Cotizar
            </Boton>
        </form>
     );
}
 
export default Formulario;